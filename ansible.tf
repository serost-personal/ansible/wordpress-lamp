
resource "null_resource" "configure_ansible" {
  triggers = {
    # run everytime
    run_everytime = timestamp()
  }

  # this shit will ensure that connection can be established
  # before starting ansible. More:
  # https://stackoverflow.com/questions/62403030/terraform-wait-till-the-instance-is-reachable
  provisioner "remote-exec" {
    connection {
      host        = aws_instance.hello_world.public_ip
      user        = "ubuntu"
      private_key = file(var.ansible_pri_key_path)
    }

    inline = ["echo Connection can be established, starting ansible..."]
  }

  provisioner "local-exec" {
    command = <<-EOT
      ANSIBLE_HOST_KEY_CHECKING=False \
      ansible all --key-file ${var.ansible_pri_key_path} \
      -i ${local_file.ansible_inventory.filename} \
      -m ping --user ubuntu
    EOT
  }
}

