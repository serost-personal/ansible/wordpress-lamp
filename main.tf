
provider "aws" {
  region = "eu-north-1"
}

variable "ansible_pub_key_path" {}
variable "ansible_pri_key_path" {}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "hello_world" {
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = "t3.micro"
  key_name               = aws_key_pair.hello_world.key_name
  vpc_security_group_ids = [aws_security_group.hello_world.id]

  depends_on = [aws_security_group.hello_world, aws_key_pair.hello_world]
}

resource "local_file" "ansible_inventory" {
  filename = "${path.module}/inventory"
  content = templatefile("${path.module}/inventory.tpl", {
    public_ips = [aws_instance.hello_world.public_ip]
  })
}

resource "aws_key_pair" "hello_world" {
  key_name   = "ansible_pub_key"
  public_key = file(var.ansible_pub_key_path)
}

resource "aws_security_group" "hello_world" {
  name        = "hello_world_allow_ssh"
  description = "Allows ssh for ansible"
  lifecycle {
    create_before_destroy = true
  }

  ingress {
    description      = "ftp"
    from_port        = 21
    to_port          = 21
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    description      = "pasv-ftp"
    from_port        = 10090
    to_port          = 10100
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    description      = "ssh from vpc"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    description      = "http"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    description      = "https"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}



output "server_ip" {
  value = aws_instance.hello_world.public_ip
}
